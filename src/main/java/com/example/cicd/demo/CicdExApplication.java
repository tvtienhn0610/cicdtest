package com.example.cicd.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CicdExApplication {

	public static void main(String[] args) {
		SpringApplication.run(CicdExApplication.class, args);
	}

}
